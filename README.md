# PrismaNote Productnumber Validation
`v0.1`
by Erik Klop - erik@prismanote.com

```
npm install
npm run dev
```

## Project proposal
**Work in progress**
## The problem

Users have to choose the right product number format in the purchase orders, or we’ll end up with a lot of duplicates. They do, however, not know this format for it’s only used in PrismaNote.
We have to come up with a way to guide the user to choose the right format, without forcing them to know the right starting number.

### Proposed solution

We can parse the users' input while they enter the product number, and give them suggestions for using the right format. (kind of like Google does when you start typing a part of a question, and Google suggests possible endings of that questions)

After brainstorming for a while, me and Jolmer came up with following approach.
In short, we would like to do this:

1. In the admin area, we need the ability to store formats per brand.

2. These formats are written as Regular Expressions, manually created to fit the product number possibilities of that brand. Regular Expressions have a bit of a learning curve, but are broadly used and fully integrated into JavaScript.

3. Since a purchase order is linked to a retailer, and the retailer to one or more brands, we know which formats have to be loaded when a user chooses a supplier in their purchase order.

4. Then, when the user is typing a search query in a purchase order, his input is checked against the formats and then suggests to ‘correct’ their search query.
-1. Kasius uses a 7-digit product number, like 4300443
-2. We have the prefix ‘KA' for Kasius, so this product is probably known as ‘KA4300443’
-3. So if the user has typed 7 digits, we detect Kasius can suggest something like: KA4300443
-4. If he clicks on a suggestions, his search query will 

### Storing formats
A format often has multiple conditions, and by splitting the format into multiple conditions, the user doesn’t have to type the whole product number to get feedback.

```
Brand
    validation_formats[]
        (Object)*
            active:boolean
            starting_digits:string
            name:string
            description:string
            conditions:array
            description:string
            regex:string
            must_match:bool
```

### Object example*
```
{
  "active":true, //whether the format is checked
  "starting_digits":"", //starting digits
  "example":"P1400",
  "conditions":[
    {
      "description":"PNV_MUST_START_WITH_P",
      "regex":"^[P]{1}",
      "must_match":true //this condition must be met
    },
    {
      "regex":"[\\d]{4}$",
      "description":"Should end with 4 digits",
      "must_match":false
    }
  ]
}
```
- starting_digits: starting digits for this brand, if present
- example: an example of this format to be shown to the user
- active: whether the prod.nr is checked against this part.
- conditions: array, see above example
- description: describe which condition is checked with this format
- regex: the actual Regular Expression
- must_match: if true, then not passing this condition discards the whole format.