var gulp = require('gulp')
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');

gulp.task('compile-npm', function() {
	return gulp.src([
		'node_modules/angular/angular.min.js',
		'node_modules/lodash/lodash.min.js'
	])
	.pipe(concat('modules.js'))
	.pipe(gulp.dest('dist/js'))
});

gulp.task('compile-js', function() {
	return gulp.src('app/js/*.js')
		.pipe(concat('app.js'))
	 	//.pipe(uglify().on('error', function(e){console.log(e);}))
		.pipe(gulp.dest('dist/js'))
});

gulp.task('compile-css', function() {
	return gulp.src('app/css/*')
		.pipe(concat('styles.css'))
		.pipe(uglifycss({ "uglyComments": true}))
		//.pipe(sass()) // Converts Sass to CSS with gulp-sass
		.pipe(gulp.dest('dist/css'))
});

gulp.task('compile-fonts', function() {
	return gulp.src('app/css/webfonts/*')
		//.pipe(concat('styles.css'))
		//.pipe(uglifycss({ "uglyComments": true}))
		//.pipe(sass()) // Converts Sass to CSS with gulp-sass
		.pipe(gulp.dest('dist/css/webfonts'))
});