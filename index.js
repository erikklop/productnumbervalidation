/**
 * Required External Modules
 */
const express = require("express");
const path = require("path");

/**
 * App Variables
 */
const app = express();
const port = process.env.PORT || "8000";

const format_data = require(path.join(__dirname, '/app/json/formats.json'))

/**
 *  App Configuration
 */
app.set("views", path.join(__dirname, "app/views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, '/dist')));

/**
 * Routes Definitions
 */
app.get("/", (req, res) => {
	//res.render("index");
	res.render('index', {
		root: path.join(__dirname, '/dist'),
		formats:format_data
	});
	//res.sendFile('views/index.html')
});

app.get("/formats", (req,res) => {
	res.send(format_data)
})

/**
 * Server Activation
 */
app.listen(port, () => {
	console.log(`Listening to requests on http://localhost:${port}`);
});

module.exports.format_data = format_data